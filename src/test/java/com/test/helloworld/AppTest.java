package com.test.helloworld;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void hopaTest()
    {
        assertEquals( "Hopa".length() , 4 );
    }
    
    
    @Test
    public void youpiTest()
    {
        assertEquals( "Youpi".length() , 5 );
    }
}
